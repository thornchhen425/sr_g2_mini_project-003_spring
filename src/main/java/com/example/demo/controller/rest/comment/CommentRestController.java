package com.example.demo.controller.rest.comment;


import com.example.demo.model.Comment;
import com.example.demo.payload.request.CommentRequest;
import com.example.demo.payload.request.ReplyRequest;
import com.example.demo.payload.response.Response;
import com.example.demo.service.CommentService.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class CommentRestController {

    @Autowired
    private CommentService commentService;

    @GetMapping("/posts/{postId}/comments")
    public Response<List<Comment>> getAllComment(@PathVariable Integer postId){
        return Response.<List<Comment>> ok().setPayload(commentService.getCommentByPostId(postId));
    }

    @GetMapping("/comments/{commentId}/replies")
    public Response<List<Comment>> getAllRepliesByCommentId(@PathVariable Integer commentId){
        return Response.<List<Comment>> ok().setPayload(commentService.getAllRepliesByCommentId(commentId));
    }

    @PostMapping("/comments")
    public Response<Comment> postComment(@RequestBody CommentRequest commentRequest){
        String user = SecurityContextHolder.getContext().getAuthentication().getName();
        Comment comment = new Comment();
        comment.setCaption(commentRequest.getContent());
        comment.setPostId(commentRequest.getPostId());
        comment.setUserId(commentService.getUserIdByUsername(user));
        if (commentService.postComment(comment))
            return Response.<Comment> ok().setPayload(comment).setErrors("Comment post Success!");
        return Response.<Comment> ok().setErrors("Comment UnSuccess!");
    }

    @PostMapping("/replies")
    public Response<Comment> replyComment(@RequestBody ReplyRequest replyRequest){
        String user = SecurityContextHolder.getContext().getAuthentication().getName();
        Comment comment = new Comment();
        comment.setCaption(replyRequest.getContent());
        comment.setPostId(replyRequest.getPostId());
        comment.setParentId(replyRequest.getParentId());
        comment.setUserId(commentService.getUserIdByUsername(user));
        if (commentService.postReply(comment))
            return Response.<Comment> ok().setPayload(comment).setErrors("Reply post Success!");
        return Response.<Comment> badRequest().setErrors("Reply UnSuccess!");
    }
}

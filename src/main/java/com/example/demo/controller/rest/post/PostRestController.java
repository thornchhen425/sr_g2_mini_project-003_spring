package com.example.demo.controller.rest.post;


import com.example.demo.model.Post;


import com.example.demo.payload.dto.post.PostDto;
import com.example.demo.payload.mapper.PostMapper;
import com.example.demo.payload.request.PostRequest;
import com.example.demo.payload.response.Response;
import com.example.demo.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.demo.model.UserLikePost;

import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping("/api/posts")
public class PostRestController {


    @Autowired
    private PostMapper postMapper;
    @Autowired
    private PostRepository postRepository;

    @GetMapping("/{id}/view")
    public Response<PostDto> findPostById(@PathVariable int id){
        Post post = postRepository.findPostById(id);
        if(post==null){
            return Response
                    .<PostDto>notFound()
                    .setErrors("Post with id "+id+" not found");
        }
        PostDto postDto = postMapper.postToPostDto(post);
        return Response.<PostDto>ok().setPayload(postDto);
    }

    @PostMapping("/create")
    public Response<PostDto> createPost(@RequestBody PostRequest postRequest){

        Post post = postMapper.postRequestToPost(postRequest);
        boolean isCreated = postRepository.createPost(post);
        if(isCreated){
            PostDto postDto = postMapper.postToPostDto(post);
            return Response.<PostDto>ok().setPayload(postDto);
        }
        return Response
                .<PostDto>badRequest()
                .setErrors("Check your data");
    }

    @GetMapping
    public Response<PostDto> findAll(){
        Post post = postRepository.findAll();
        if(post==null){
            return Response
                    .<PostDto>notFound()
                    .setErrors("Post not found");
        }
        PostDto postDto = postMapper.postToPostDto(post);
        return Response.<PostDto>ok().setPayload(postDto);
    }

    


//    @PatchMapping("/react/{id}")
//    public Map<String, Object> react(@PathVariable int id, Enum.LikeType likeType) {
//
//        Map<String, Object> response = new HashMap<>();
//
//        response.put("status", "OK");
//        response.put("success", true);
//        return response;
//    }
//    @GetMapping
//    public ResponseEntity<Map<String, Object>> findAllPost() {
//        Map<String, Object> response = new HashMap<>();
//        Post post = new Post();
//        post.setId(1);
//        post.setCaption("1");
//        post.setImageUrl("1");
//        post.setNumberOfLikes(1);
//        post.setUserId(3);
//        post.setUsername("Mocha");
//        post.setOwner(false);
//
//        response.put("payload", post);
//        response.put("status", "OK");
//        return ResponseEntity.ok(response);
//    }
//
//    @GetMapping("/{id}/view")
//    public ResponseEntity<Map<String, Object>> findViewById(@PathVariable int id) {
//        Map<String, Object> response = new HashMap<>();
//
//        PostDto postDto = new PostDto();
//        postDto.setId(id);
//        postDto.setCaption("1");
//        postDto.setImageUrl("1");
//        postDto.setUserId(3);
//        postDto.setNumberOfLikes(1);
//        postDto.setUsername("string");
//
//        response.put("status", "OK");
//        response.put("payload", postDto);
//        response.put("success", true);
//        return ResponseEntity.ok().body(response);
//    }

}

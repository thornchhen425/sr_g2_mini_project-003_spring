package com.example.demo.model;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class Comment {

    private int id;
    private String caption;
    private int parentId;
    private int postId;
    private int userId;
    private User users;
    private List<Comment> replies;


}

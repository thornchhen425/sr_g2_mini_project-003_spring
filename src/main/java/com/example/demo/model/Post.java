package com.example.demo.model;


import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Post {

    private int id;
    private String caption;
    private int numberOfLikes;
    private String image;
    private boolean owner;
    private List<Comment> comments;
}

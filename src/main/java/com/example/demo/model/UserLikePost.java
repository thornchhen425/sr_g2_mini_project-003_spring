package com.example.demo.model;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UserLikePost {
    public enum LikeType{
        Like("Like"),
        Dislike("Dislike");
        private String str;
        LikeType(String str){
            this.str = str;

    }
    public String getStr(){
        return str;
        }
    }
//    private int userId;
//    private int postId;
}

package com.example.demo.model;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UserRole {

    private int userId;
    private int roleId;
}

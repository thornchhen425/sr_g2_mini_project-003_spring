package com.example.demo.payload.dto.post;

import com.example.demo.model.Comment;
import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class PostDto {
    private int id;
    private String caption;
    private int numberOfLike;
    private String image;
    private boolean owner;
}

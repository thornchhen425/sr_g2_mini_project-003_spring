package com.example.demo.payload.mapper;

import com.example.demo.model.Post;
import com.example.demo.payload.dto.post.PostDto;
import com.example.demo.payload.request.PostRequest;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PostMapper {

    Post postDtoToPost(PostDto postDto);
    PostDto postToPostDto(Post post);

    Post postRequestToPost(PostRequest postRequest);
}

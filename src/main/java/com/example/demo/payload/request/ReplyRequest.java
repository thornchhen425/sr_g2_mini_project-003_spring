package com.example.demo.payload.request;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ReplyRequest {

    private String content;
    private int parentId;
    private int postId;
}

package com.example.demo.payload.response;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class JwtResponse {
  private long id;
  private String token;
  private String fullName;
  private String username;
  private String email;
}
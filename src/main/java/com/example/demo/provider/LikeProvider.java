package com.example.demo.provider;

import org.apache.ibatis.jdbc.SQL;

public class LikeProvider {
    public String selectPost(
            String caption, Integer id, String username
    ) {
        SQL sql = new SQL() {
            {
                SELECT(
                        "posts.id as post_id",
                        "caption",
                        "image",
                        "no_of_likes",
                        "user_id",
                        "username"
                );
                FROM("posts");
                INNER_JOIN("users ON posts.user_id = users.id");
                if (username != null) {
                    WHERE("username = #{username}");
                }
                if (caption != null) {
                    OR();
                    WHERE("caption LIKE '%'||#{caption}||'%' ");
                }
                if (id != null) {
                    OR();
                    WHERE("user_id = #{id}");
                }
                ORDER_BY("post_id DESC");
                LIMIT("#{limit}");
                OFFSET("(#{page} - 1) * #{limit}");
            }
        };
        return sql.toString();
    }
}

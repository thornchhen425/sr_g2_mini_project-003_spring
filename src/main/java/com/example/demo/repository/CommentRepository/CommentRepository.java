package com.example.demo.repository.CommentRepository;


import com.example.demo.model.Comment;
import com.example.demo.model.User;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface CommentRepository {

    ///Start Get All Comments By Post ID
    @Select("select * from comments where post_id=#{postId}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "postId", column = "post_id"),
            @Result(property = "parentId", column = "parent_id"),
            @Result(property = "replies", column = "id", many = @Many(select = "getReplies")),
            @Result(property = "users",
                    javaType = User.class,
                    column = "user_id",
                    one = @One(select = "getUser")
            )
    })
    List<Comment> getCommentByPostId(int postId);

    @Select("select * from users where id=#{userId}")
    User getUser(int userId);

    @Select("select * from comments where parent_id=#{id}")
    @Results({
            @Result(property = "postId", column = "post_id"),
            @Result(property = "parentId", column = "parent_id")
    })
    List<Comment> getReplies(int parent_id);

    ///End Get All Comments By Post ID


    ///Start Get All Replies By Comment ID
    @Select("select * from comments where parent_id=#{commentId}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "postId", column = "post_id"),
            @Result(property = "parentId", column = "parent_id"),
            @Result(property = "users",
                    javaType = User.class,
                    column = "user_id",
                    one = @One(select = "getUser")
            )
    })
    List<Comment> getAllRepliesByCommentId(int commentId);

    ///End Get All Replies By Comment ID


    ///Start Post Comment/Reply
    @Insert("INSERT INTO comments(caption,post_id,user_id) VALUES(#{caption},#{postId},#{userId})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    boolean postComment(Comment comment);

    @Insert("INSERT INTO comments(caption,post_id,user_id) VALUES(#{caption},#{postId},#{userId})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    boolean postReply(Comment comment);

    @Select("SELECT TOP 1 id FROM users WHERE username LIKE #{username}")
    Integer getUserIdByUsername(String username);

}


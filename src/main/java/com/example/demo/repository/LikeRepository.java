package com.example.demo.repository;

import com.example.demo.provider.LikeProvider;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import com.example.demo.model.Post;

@Mapper
public interface LikeRepository {
    @SelectProvider(type = LikeProvider.class, method = "selectPost")
    @Results(value = {
            @Result(property = "postId", column = "post_id"),
            @Result(property = "caption", column = "caption"),
            @Result(property = "imageUrl", column = "image"),
            @Result(property = "numberOfLikes", column = "no_of_likes"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "username", column = "username")
    })
    List<Post> findPosts(
            Integer limit,
            Integer page,
            String caption,
            Integer id,
            String username
    );
}


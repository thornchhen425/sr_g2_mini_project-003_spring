package com.example.demo.repository;

import com.example.demo.model.Post;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface PostRepository {

    @Select("select * from posts where id = #{id}")
    Post findPostById(int id);

    @Select("select *from posts")
    Post findAll();

    @Insert("insert into posts (content , image) values (#{caption},#{image})")
    @Result(property = "caption",column = "content")
    boolean createPost(Post post);
}

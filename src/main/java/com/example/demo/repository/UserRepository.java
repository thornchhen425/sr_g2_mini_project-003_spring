package com.example.demo.repository;
import com.example.demo.model.User;
import java.util.List;
import org.apache.ibatis.annotations.*;
import com.example.demo.model.Role;

@Mapper
public interface UserRepository {
    @Select("SELECT * FROM users;")
    @Result(property = "roles",
            column = "role_id",
            many = @Many(select = "getRolesByUserId")
    )
    List<User> getUsers();

    @Select("SELECT * FROM users WHERE username=#{username};")
    @Result(property = "roles",
            column = "role_id",
            many = @Many(select = "getRolesByUserId")
    )
    User getUserByUsername(String username);

    @Select("SELECT r.id as id, " +
            "r.name as name " +
            "FROM user_roles as ur " +
            "INNER JOIN roles as r " +
            "ON r.id = ur.role_id " +
            "WHERE user_id = #{id};")
    List<Role> getRolesByUserId(Integer id);

    @Insert("INSERT INTO users (" +
            "fullname, username, password" +
            ")" +
            "VALUES (" +
            "#{fullname},#{username},#{password}" +
            ");")
    Integer insertNewUser(
            String fullname,
            String username,
            String password
    );

    @Insert("INSERT INTO user_roles (user_id, role_id)" +
            "VALUES (#{userId},#{roleId})" +
            ";")
    int insertUserRole(
            Integer userId,
            Integer roleId
    );
}


package com.example.demo.security;

import com.example.demo.model.Role;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CurrentUser {
  private int id;
  private String username;
  private String fullName;
  private List<Role> roles;
}

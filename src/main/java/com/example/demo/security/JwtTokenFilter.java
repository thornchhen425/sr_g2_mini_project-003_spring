package com.example.demo.security;

import com.example.demo.service.UserDetailsServiceImpl;
import com.example.demo.util.MapperUtils;
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

public class JwtTokenFilter extends OncePerRequestFilter {

  @Autowired
  private CurrentUser currentUser;

  @Autowired
  private MapperUtils mapperUtils;

  @Autowired
  private JwtUtils jwtUtils;

  @Autowired
  private UserDetailsServiceImpl userDetailsService;

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
      FilterChain filterChain) throws ServletException, IOException {
    var token = parseToken(request);

    // Verify token
    if(token != null && jwtUtils.validateToken(token)){
      var subject = jwtUtils.getSubjectFromToken(token);

      // Load user from service
      var userDetails = userDetailsService.loadUserByUsername(subject);

      // Set current user
      mapperUtils.copy(userDetails,currentUser);

      // Authorization
      UsernamePasswordAuthenticationToken authentication =
          new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
      authentication
          .setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
      SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    else{
      // Set current user
      mapperUtils.copy(new CurrentUser(),currentUser);

    }
    filterChain.doFilter(request,response);

  }
  private String parseToken(HttpServletRequest request){
    var header= request.getHeader("Authorization");
    var prefix = "Bearer";
    if(StringUtils.hasText(header) && header.startsWith(prefix)){
      return header.substring(prefix.length());
    }
    return null;
  }
}

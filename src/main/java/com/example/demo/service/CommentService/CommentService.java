package com.example.demo.service.CommentService;


import com.example.demo.model.Comment;

import java.util.List;

public interface CommentService {

    List<Comment> getCommentByPostId(int postId);

    List<Comment> getAllRepliesByCommentId(int commentId);

    boolean postComment(Comment comment);

    boolean postReply(Comment comment);

    Integer getUserIdByUsername(String username);

}

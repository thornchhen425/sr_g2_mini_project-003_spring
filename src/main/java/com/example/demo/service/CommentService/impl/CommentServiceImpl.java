package com.example.demo.service.CommentService.impl;

import com.example.demo.model.Comment;
import com.example.demo.repository.CommentRepository.CommentRepository;
import com.example.demo.service.CommentService.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentRepository commentRepository;

    @Override
    public List<Comment> getCommentByPostId(int postId) {
        return commentRepository.getCommentByPostId(postId);
    }

    @Override
    public List<Comment> getAllRepliesByCommentId(int commentId) {
        return commentRepository.getAllRepliesByCommentId(commentId);
    }

    @Override
    public boolean postComment(Comment comment) {
        commentRepository.postComment(comment);
        return true;
    }

    @Override
    public boolean postReply(Comment comment) {
        commentRepository.postReply(comment);
        return true;
    }

    @Override
    public Integer getUserIdByUsername(String username) {
        return commentRepository.getUserIdByUsername(username);
    }
}

package com.example.demo.service;

import com.example.demo.payload.dto.post.PostDto;

import java.util.List;

public interface PostService {
    public List<PostDto> findAll();

    public PostDto findPostById(Integer id);
    public boolean delete(Integer id);
}

package com.example.demo.service;

import com.example.demo.model.UserDetailsImpl;
import com.example.demo.util.MapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

  @Autowired
  private UserService userService;
  @Autowired
  private MapperUtils mapperUtils;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    var user = userService.findUserByUsername(username);
    if(user != null)
      return mapperUtils.map(user, UserDetailsImpl.class);
    return null;
  }
}

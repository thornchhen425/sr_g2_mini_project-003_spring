package com.example.demo.util;

import org.springframework.stereotype.Component;
import org.modelmapper.ModelMapper;
@Component
public class MapperUtils {
    private final ModelMapper modelMapper;

    MapperUtils(){
      modelMapper = new ModelMapper();
    }
  public <T> T map(Object source, Class<T> type) {
    return modelMapper.map(source, type);
  }

  public void copy(Object source, Object target) {
    modelMapper.map(source, target);
  }
}


create table roles
(
    id serial not null
        constraint roles_pk
            primary key,
    role varchar not null
);

create table users
(
    id serial not null,
    username varchar not null,
    password varchar not null,
    image varchar
);

create table posts
(
    id             serial not null,
    content        varchar,
    numberOfLike integer,
    owner          boolean,
    image          varchar
);


create table comments
(
	id serial not null,
	caption varchar,
	parent_id int,
	post_id int
		constraint comments_posts_id_fk
			references posts (id),
	user_id int
		constraint comments_users_id_fk
			references users (id)
);

create table user_like_post
(
	user_id int,
	post_id int
);
create table user_role
(
	user_id int
		constraint user_role_users_id_fk
			references users (id),
	role_id int
		constraint user_role_roles_id_fk
			references roles
);